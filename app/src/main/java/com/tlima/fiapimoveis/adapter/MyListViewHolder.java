package com.tlima.fiapimoveis.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.tlima.fiapimoveis.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MyListViewHolder extends RecyclerView.ViewHolder {

    @Bind(R.id.image)
    protected ImageView image;
    @Bind(R.id.firstLine)
    protected TextView firstLine;

    protected long id;

    public MyListViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public ImageView getImage() {
        return image;
    }

    public void setImage(ImageView image) {
        this.image = image;
    }

    public TextView getFirstLine() {
        return firstLine;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
