package com.tlima.fiapimoveis.adapter;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.tlima.fiapimoveis.R;
import com.tlima.fiapimoveis.activity.DetailsActivity;
import com.tlima.fiapimoveis.app.Application;
import com.tlima.fiapimoveis.model.Property;

import java.util.List;

public class MyRecyclerViewAdapter extends RecyclerView.Adapter<MyListViewHolder> {

    private final List<Property> properties;

    public MyRecyclerViewAdapter(List<Property> properties) {
        this.properties = properties;
    }

    @Override
    public MyListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        final View view = inflater.inflate(R.layout.cardview, parent, false);
        final MyListViewHolder holder = new MyListViewHolder(view);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(view.getContext(), DetailsActivity.class);
                intent.putExtra("propertyId", holder.getId());
                view.getContext().startActivity(intent);
            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(MyListViewHolder holder, int position) {
        Property property = properties.get(position);
        holder.setId(property.getId());

        if (property.getImage() != null) {
            ImageLoader imageLoader = ImageLoader.getInstance();
            imageLoader.displayImage(property.getImage(), holder.getImage());
        }

        holder.getFirstLine().setText(Application.getPrimaryInfo(property));
    }

    @Override
    public int getItemCount() {
        return properties.size();
    }

}
