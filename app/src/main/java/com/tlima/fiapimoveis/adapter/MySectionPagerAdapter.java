package com.tlima.fiapimoveis.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

public class MySectionPagerAdapter extends FragmentPagerAdapter {

    private final List<Fragment> sections;

    public MySectionPagerAdapter(FragmentManager fragmentManager, List<Fragment> sections) {
        super(fragmentManager);
        this.sections = sections;
    }

    @Override
    public Fragment getItem(int position) {
        return sections.get(position);
    }

    @Override
    public int getCount() {
        return sections.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return position == 0 ? "Properties" : "Favorites";
    }
}
