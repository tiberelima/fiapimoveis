package com.tlima.fiapimoveis.app;

import android.content.Context;
import android.content.SharedPreferences;

import com.activeandroid.ActiveAndroid;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.tlima.fiapimoveis.R;
import com.tlima.fiapimoveis.model.Property;

import java.util.ArrayList;
import java.util.List;

public class Application extends com.activeandroid.app.Application {
    public static final String APP_NAME = "FiapImoveis";
    public static final String LOGOFF = "logoff";
    public static final String PREFERENCE = "app_preferences";
    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";
    public static final String KEEP_CONNECTED = "keep_connected";

    public static SharedPreferences preferences;
    private static Application instance;

    public static SharedPreferences getPreferences() {
        return preferences;
    }

    public static String getPrimaryInfo(Property property) {

        String state = instance.getString(instance.getResources().getIdentifier(property.getState().name().toLowerCase(), "string", instance.getPackageName()));
        String size = instance.getString(instance.getResources().getIdentifier(property.getSize().name().toLowerCase(), "string", instance.getPackageName()));
        String type = instance.getString(instance.getResources().getIdentifier(property.getType().name().toLowerCase(), "string", instance.getPackageName()));

        return (type + ", " + size + ", " + state);
    }

    public static List<String> getListInternationalized(List<String> values) {
        List<String> result = new ArrayList<>();
        for (String value : values) {
            result.add(instance.getString(instance.getResources().getIdentifier(value.toLowerCase(), "string", instance.getPackageName())));
        }
        return result;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        ActiveAndroid.initialize(this);
        instance = this;


        preferences = getSharedPreferences(Application.PREFERENCE, Context.MODE_PRIVATE);

        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .showImageOnLoading(R.drawable.banner_android)
                .showImageForEmptyUri(R.drawable.banner_android)
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this)
                .defaultDisplayImageOptions(options)
                .build();

        ImageLoader.getInstance().init(config);
    }
}
