package com.tlima.fiapimoveis.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.activeandroid.query.From;
import com.activeandroid.query.Select;
import com.tlima.fiapimoveis.R;
import com.tlima.fiapimoveis.adapter.MyRecyclerViewAdapter;
import com.tlima.fiapimoveis.model.Property;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class CardViewListFragment extends Fragment {

    @Bind(R.id.rvList)
    protected RecyclerView rvList;
    private LinearLayoutManager linearLayoutManager;
    private MyRecyclerViewAdapter myRecyclerViewAdapter;
    private boolean favorites;
    private List<Property> properties;

    public void reloadList(boolean favorites) {
        reloadList(getProperties(favorites));
    }

    public void reloadList(List<Property> newProperties) {
        properties.clear();
        properties.addAll(newProperties);
        myRecyclerViewAdapter.notifyDataSetChanged();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle arguments = getArguments();

        if (arguments != null) {
            favorites = arguments.getBoolean("favorites", false);
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_cardview_list, container, false);
        ButterKnife.bind(this, view);

        properties = getProperties(favorites);

        linearLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        myRecyclerViewAdapter = new MyRecyclerViewAdapter(properties);

        rvList.setLayoutManager(linearLayoutManager);
        rvList.setAdapter(myRecyclerViewAdapter);

        return view;
    }

    private List<Property> getProperties(boolean favorites) {
        List<Property> properties;
        From from = new Select().from(Property.class).where("deleted = ?", false);

        if (favorites) {
            from = from.where("favorite = ? ", favorites);
        }

        properties = from.execute();
        return properties;
    }
}