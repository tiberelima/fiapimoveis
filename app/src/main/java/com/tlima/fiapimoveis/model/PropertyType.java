package com.tlima.fiapimoveis.model;

import java.util.ArrayList;
import java.util.List;

public enum PropertyType {
    UNKNOWN(0),
    HOUSE(1),
    APARTMENT(2),
    SHOP(3);
    private final int position;

    PropertyType(int position) {
        this.position = position;
    }

    public static PropertyType getByPosition(int position) {
        for (PropertyType type : PropertyType.values()) {
            if (type.getPosition() == position) {
                return type;
            }
        }
        return null;
    }

    public static List<String> valuesAsString() {
        List<String> valuesAsString = new ArrayList<>();
        for (PropertyType type : PropertyType.values()) {
            valuesAsString.add(type.name());
        }
        return valuesAsString;
    }

    public int getPosition() {
        return position;
    }
}
