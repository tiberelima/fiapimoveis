package com.tlima.fiapimoveis.model;

import java.util.ArrayList;
import java.util.List;

public enum Size {
    UNKNOWN(0),
    SMALL(1),
    MEDIUM(2),
    LARGE(3);

    private final int position;

    Size(int position) {
        this.position = position;
    }

    public static List<String> valuesAsString() {
        List<String> valuesAsString = new ArrayList<>();
        for (Size size : Size.values()) {
            valuesAsString.add(size.name());
        }
        return valuesAsString;
    }

    public static Size getByPosition(int position) {
        for (Size size : Size.values()) {
            if (size.getPosition() == position) {
                return size;
            }
        }
        return null;
    }

    public int getPosition() {
        return position;
    }
}
