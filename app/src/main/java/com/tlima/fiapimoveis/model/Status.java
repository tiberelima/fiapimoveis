package com.tlima.fiapimoveis.model;

import java.util.ArrayList;
import java.util.List;

public enum Status {
    READY_TO_LIVE(0),
    UNDER_CONSTRUCTION(1);

    private final int position;

    Status(int position) {
        this.position = position;
    }

    public static List<String> valuesAsString() {
        List<String> valuesAsString = new ArrayList<>();
        for (Status status : Status.values()) {
            valuesAsString.add(status.name());
        }
        return valuesAsString;
    }

    public static Status getByPosition(int position) {
        for (Status status : Status.values()) {
            if (status.getPosition() == position) {
                return status;
            }
        }
        return null;
    }

    public int getPosition() {
        return position;
    }
}

