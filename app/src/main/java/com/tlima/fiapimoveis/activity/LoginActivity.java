package com.tlima.fiapimoveis.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.activeandroid.query.Select;
import com.tlima.fiapimoveis.R;
import com.tlima.fiapimoveis.app.Application;
import com.tlima.fiapimoveis.model.User;

import butterknife.Bind;
import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity {

    @Bind(R.id.username)
    protected EditText username;
    @Bind(R.id.password)
    protected EditText password;
    @Bind(R.id.keepConnected)
    protected CheckBox keepConnected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        if (getIntent().getBooleanExtra(Application.LOGOFF, false)) {
            SharedPreferences.Editor editor = Application.getPreferences().edit();
            editor.putBoolean(Application.KEEP_CONNECTED, false);
            editor.commit();
            finish();
        }
        checkKeepConnected();
    }

    private void checkKeepConnected() {
        if (Application.getPreferences().getBoolean(Application.KEEP_CONNECTED, false)) {
            User user = getUserRegistered(
                    Application.getPreferences().getString(Application.USERNAME, ""),
                    Application.getPreferences().getString(Application.PASSWORD, ""));
            redirectOrAlert(user, true);
        }
    }

    public void login(View view) {
        User user = getUserRegistered(username.getText().toString(), password.getText().toString());
        redirectOrAlert(user, keepConnected.isChecked());
    }

    private void redirectOrAlert(User user, boolean keepConnected) {
        if (user != null) {
            saveUserData(user, keepConnected);

            startActivity(new Intent(this, MainActivity.class));
            finish();
        } else {
            Toast.makeText(this, R.string.login_fail, Toast.LENGTH_LONG).show();
        }
    }

    private void saveUserData(User user, boolean keepConnected) {
        SharedPreferences.Editor editor = Application.getPreferences().edit();
        editor.putString(Application.USERNAME, user.getUserName());
        editor.putString(Application.PASSWORD, user.getPassword());
        editor.putBoolean(Application.KEEP_CONNECTED, keepConnected);
        editor.commit();
    }

    private User getUserRegistered(String username, String password) {
        return new Select()
                .from(User.class)
                .where("username = ? and password = ?", username, password)
                .executeSingle();
    }

    public void register(View view) {
        startActivity(new Intent(this, RegisterActivity.class));
    }
}
