package com.tlima.fiapimoveis.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Switch;

import com.activeandroid.query.From;
import com.activeandroid.query.Select;
import com.tlima.fiapimoveis.R;
import com.tlima.fiapimoveis.app.Application;
import com.tlima.fiapimoveis.model.Property;
import com.tlima.fiapimoveis.model.PropertyType;
import com.tlima.fiapimoveis.model.Size;
import com.tlima.fiapimoveis.model.Status;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FindActivity extends AppCompatActivity {

    @Bind(R.id.property_type)
    protected Spinner propertyType;
    @Bind(R.id.property_size)
    protected Spinner propertySize;
    @Bind(R.id.property_under_construction)
    protected Switch underConstruction;
    private Size sizeSelected;
    private PropertyType typeSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_find);
        ButterKnife.bind(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ArrayAdapter<String> propertyTypeAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, Application.getListInternationalized(PropertyType.valuesAsString()));
        propertyTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        propertyType.setAdapter(propertyTypeAdapter);
        propertyType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                typeSelected = PropertyType.getByPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter<String> propertySizeAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, Application.getListInternationalized(Size.valuesAsString()));
        propertySizeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        propertySize.setAdapter(propertySizeAdapter);
        propertySize.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                sizeSelected = Size.getByPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @OnClick(R.id.fab_find)
    public void findClick() {

        From from = new Select().from(Property.class);
        from.where("deleted = ?", false);
        from.where("type = ?", typeSelected);
        from.where("size = ?", sizeSelected);
        from.where("state = ?", underConstruction.isChecked()
                ? Status.UNDER_CONSTRUCTION
                : Status.READY_TO_LIVE);

        List<Property> properties = from.execute();

        MainActivity.instance.reloadSectionData(false, properties);
        finish();
    }
}
