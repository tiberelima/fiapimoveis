package com.tlima.fiapimoveis.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.tlima.fiapimoveis.R;
import com.tlima.fiapimoveis.model.User;

import butterknife.Bind;
import butterknife.ButterKnife;

public class RegisterActivity extends AppCompatActivity {

    @Bind(R.id.register)
    protected Button register;
    @Bind(R.id.name)
    protected EditText name;
    @Bind(R.id.username)
    protected EditText userName;
    @Bind(R.id.password)
    protected EditText password;
    @Bind(R.id.confirmPassword)
    protected EditText confirmPassword;
    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        publishListeners();
    }

    private void publishListeners() {
        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                user = new User();
                user.setName(name.getText().toString());
                user.setUserName(userName.getText().toString());
                user.setPassword(password.getText().toString());

                String confirmation = confirmPassword.getText().toString();

                if (isValid(user)) {
                    if (user.getPassword().equals(confirmation)) {
                        user.save();

                        Snackbar.make(v, R.string.registration_success, Snackbar.LENGTH_INDEFINITE)
                                .setAction(R.string.proceed, new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        startActivity(new Intent(getApplicationContext(), MainActivity.class));
                                    }
                                }).show();
                    } else {
                        Toast.makeText(getApplicationContext(), R.string.check_password, Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getApplicationContext(), R.string.incorrect_form_filled, Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private boolean isValid(User user) {
        return (!user.getName().isEmpty() && !user.getUserName().isEmpty() && !user.getPassword().isEmpty());
    }
}
