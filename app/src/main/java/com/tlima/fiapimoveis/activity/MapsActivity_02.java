package com.tlima.fiapimoveis.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.activeandroid.query.Select;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.tlima.fiapimoveis.R;
import com.tlima.fiapimoveis.model.Property;

import java.util.ArrayList;
import java.util.List;

public class MapsActivity_02 extends FragmentActivity {

    private GoogleMap mMap;
    private Property property;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps_activity_02);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            long propertyId = extras.getLong("propertyId", 0);

            property = new Select().from(Property.class).where("id = ? ", propertyId).executeSingle();
        }

        setUpMapIfNeeded();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }

    private void setUpMapIfNeeded() {
        if (mMap == null) {
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map)).getMap();
            if (mMap != null) {
                setUpMap();
            }
        }
    }

    private void setUpMap() {
        List<Property> properties = new ArrayList<>();

        if (property == null) {
            properties = new Select().from(Property.class).where("deleted = ?", false).execute();
        } else {
            properties.add(property);
        }

        LatLng latLng = new LatLng(-23.6824124, -46.5952992);
        for (Property property : properties) {
            if (property.getLatitude() != null && property.getLongitude() != null) {
                latLng = new LatLng(property.getLatitude(), property.getLongitude());
                mMap.addMarker(new MarkerOptions().position(latLng).title(property.getType().name()));
            }
        }

        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
    }
}
