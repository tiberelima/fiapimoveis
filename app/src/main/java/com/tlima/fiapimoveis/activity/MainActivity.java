package com.tlima.fiapimoveis.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.tlima.fiapimoveis.R;
import com.tlima.fiapimoveis.adapter.MySectionPagerAdapter;
import com.tlima.fiapimoveis.app.Application;
import com.tlima.fiapimoveis.fragment.CardViewListFragment;
import com.tlima.fiapimoveis.listener.MyOnNavigationItemSelectedListener;
import com.tlima.fiapimoveis.model.Property;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    public static MainActivity instance;

    @Bind(R.id.toolbar)
    protected Toolbar toolbar;
    @Bind(R.id.navigation_view)
    protected NavigationView navigationView;
    @Bind(R.id.drawer_layout)
    protected DrawerLayout drawerLayout;
    @Bind(R.id.username)
    protected TextView username;
    @Bind(R.id.tab_layout)
    protected TabLayout tabLayout;
    @Bind(R.id.pager)
    protected ViewPager viewPager;

    private List<Fragment> sections;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        MainActivity.instance = this;

        username.setText(Application.getPreferences().getString(Application.USERNAME, getString(R.string.default_username)));

        setSupportActionBar(toolbar);

        ActionBarDrawerToggle actionBarDrawerToggle =
                new MyActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.openDrawer, R.string.closeDrawer);

        drawerLayout.setDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

        navigationView.setNavigationItemSelectedListener(new MyOnNavigationItemSelectedListener(this, drawerLayout));

        sections = new ArrayList<>();

        sections.add(initializeSection(false));
        sections.add(initializeSection(true));

        viewPager.setAdapter(new MySectionPagerAdapter(getSupportFragmentManager(), sections));
        tabLayout.setupWithViewPager(viewPager);
    }

    private Fragment initializeSection(boolean favorites) {
        Fragment cardViewListFragment = new CardViewListFragment();
        Bundle arguments = cardViewListFragment.getArguments();
        if (arguments == null) {
            arguments = new Bundle();
        }

        arguments.putBoolean("favorites", favorites);
        cardViewListFragment.setArguments(arguments);

        return cardViewListFragment;
    }

    public void reloadSectionData(boolean favorites) {
        ((CardViewListFragment) sections.get(favorites ? 1 : 0)).reloadList(favorites);
    }

    public void reloadSectionData(boolean favorites, List<Property> properties) {
        ((CardViewListFragment) sections.get(favorites ? 1 : 0)).reloadList(properties);
    }

    private class MyActionBarDrawerToggle extends ActionBarDrawerToggle {

        public MyActionBarDrawerToggle(Activity activity, DrawerLayout drawerLayout, Toolbar toolbar, int openDrawerContentDescRes, int closeDrawerContentDescRes) {
            super(activity, drawerLayout, toolbar, openDrawerContentDescRes, closeDrawerContentDescRes);
        }

        @Override
        public void onDrawerClosed(View drawerView) {
            super.onDrawerClosed(drawerView);
        }

        @Override
        public void onDrawerOpened(View drawerView) {
            super.onDrawerOpened(drawerView);
        }
    }
}
