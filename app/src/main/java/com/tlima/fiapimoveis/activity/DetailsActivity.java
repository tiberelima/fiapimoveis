package com.tlima.fiapimoveis.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import com.activeandroid.query.Select;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.tlima.fiapimoveis.R;
import com.tlima.fiapimoveis.app.Application;
import com.tlima.fiapimoveis.model.Property;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class DetailsActivity extends AppCompatActivity {

    @Bind(R.id.image)
    protected ImageView image;
    @Bind(R.id.info)
    protected TextView info;
    @Bind(R.id.observation)
    protected TextView observation;
    @Bind(R.id.contact)
    protected TextView contact;
    private Property property;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        ButterKnife.bind(this);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            long propertyId = extras.getLong("propertyId", 0);

            property = new Select().from(Property.class).where("id = ? ", propertyId).executeSingle();

            if (property.getImage() != null) {
                ImageLoader imageLoader = ImageLoader.getInstance();
                imageLoader.displayImage(property.getImage(), image);
            }

            info.setText(Application.getPrimaryInfo(property));
            contact.setText(property.getContact().getName() + getString(R.string.phone) + ": " + property.getContact().getPhone());
            observation.setText(property.getObservation());
        }
    }

    @OnClick(R.id.viewOnMap)
    public void viewOnMapClick() {
        Intent intent = new Intent(this, MapsActivity_02.class);
        intent.putExtra("propertyId", property.getId());
        startActivity(intent);
    }

    @OnClick(R.id.call)
    public void callClick() {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:" + property.getContact().getPhone()));
        startActivity(intent);
    }

    @OnClick(R.id.remove)
    public void removeClick() {
        property.setDeleted(true);
        property.save();

        MainActivity.instance.reloadSectionData(false);
        MainActivity.instance.reloadSectionData(true);
        finish();
    }

    @OnClick(R.id.edit)
    public void editClick() {
        Intent intent = new Intent(this, PropertyRegisterActivity.class);
        intent.putExtra("propertyId", property.getId());
        startActivity(intent);
    }

    @OnClick(R.id.favorite)
    public void favoriteClick() {
        property.setFavorite(true);
        property.save();

        MainActivity.instance.reloadSectionData(true);
        finish();
    }
}
