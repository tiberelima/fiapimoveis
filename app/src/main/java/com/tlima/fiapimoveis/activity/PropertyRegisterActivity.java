package com.tlima.fiapimoveis.activity;

import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.activeandroid.query.Select;
import com.tlima.fiapimoveis.R;
import com.tlima.fiapimoveis.app.Application;
import com.tlima.fiapimoveis.model.Contact;
import com.tlima.fiapimoveis.model.Property;
import com.tlima.fiapimoveis.model.PropertyType;
import com.tlima.fiapimoveis.model.Size;
import com.tlima.fiapimoveis.model.Status;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PropertyRegisterActivity extends AppCompatActivity implements LocationListener {

    private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;

    @Bind(R.id.mCoordinator)
    protected CoordinatorLayout mCoordinator;
    @Bind(R.id.contact)
    protected EditText contact;
    @Bind(R.id.phone)
    protected EditText phone;
    @Bind(R.id.observation)
    protected EditText observation;
    @Bind(R.id.property_size)
    protected Spinner propertySize;
    @Bind(R.id.property_type)
    protected Spinner propertyType;
    @Bind(R.id.property_status)
    protected Spinner propertyStatus;

    private Size sizeSelected = Size.UNKNOWN;
    private Status statusSelected = Status.READY_TO_LIVE;
    private PropertyType typeSelected = PropertyType.UNKNOWN;

    private Uri fileUri;
    private Location location;
    private LocationManager locationManager;
    private String provider;
    private Property property;

    private static Uri getOutputMediaFileUri() {
        return Uri.fromFile(getOutputMediaFile());
    }

    private static File getOutputMediaFile() {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), Application.APP_NAME);

        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d(Application.APP_NAME, "failed to create directory");
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        return new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + ".jpg");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_property_register);
        ButterKnife.bind(this);

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        Criteria criteria = new Criteria();
        provider = locationManager.getBestProvider(criteria, true);
        Location location = locationManager.getLastKnownLocation(provider);
        onLocationChanged(location);

        ArrayAdapter<String> propertyTypeAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, Application.getListInternationalized(PropertyType.valuesAsString()));
        propertyTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        propertyType.setAdapter(propertyTypeAdapter);
        propertyType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                typeSelected = PropertyType.getByPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        ArrayAdapter<String> propertySizeAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, Application.getListInternationalized(Size.valuesAsString()));
        propertySizeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        propertySize.setAdapter(propertySizeAdapter);
        propertySize.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                sizeSelected = Size.getByPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        ArrayAdapter<String> propertyStatusAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, Application.getListInternationalized(Status.valuesAsString()));
        propertyStatusAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        propertyStatus.setAdapter(propertyStatusAdapter);
        propertyStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                statusSelected = Status.getByPosition(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            long propertyId = extras.getLong("propertyId", 0);
            property = new Select().from(Property.class).where("id = ? ", propertyId).executeSingle();
            contact.setText(property.getContact().getName());
            phone.setText(property.getContact().getPhone());
            observation.setText(property.getObservation());
            propertySize.setSelection(property.getSize().getPosition());
            propertyType.setSelection(property.getType().getPosition());
            propertyStatus.setSelection(property.getState().getPosition());
        }

    }

    @OnClick(R.id.capture)
    void captureImage() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        fileUri = getOutputMediaFileUri();
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
    }

    @OnClick(R.id.fabSalvar)
    void onClickSalvar() {
        Contact contactTo;

        if (property == null) {
            property = new Property();
            contactTo = new Contact();
        } else {
            contactTo = property.getContact();
        }

        contactTo.setName(contact.getText().toString());
        contactTo.setPhone(phone.getText().toString());
        contactTo.save();

        property.setContact(contactTo);
        property.setSize(sizeSelected);
        property.setState(statusSelected);
        property.setType(typeSelected);
        property.setDeleted(false);
        property.setObservation(observation.getText().toString());

        if (fileUri != null) {
            property.setImage(fileUri.toString());
        }
        if (location != null) {
            Log.i(Application.APP_NAME, location.toString());
            property.setLatitude(location.getLatitude());
            property.setLongitude(location.getLongitude());
        }

        property.save();
        MainActivity.instance.reloadSectionData(false);

        Snackbar.make(mCoordinator, R.string.registered_success, Snackbar.LENGTH_LONG)
                .setAction(R.string.undo,
                        new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                property.setDeleted(true);
                                property.save();
                                Toast.makeText(getApplicationContext(), R.string.done, Toast.LENGTH_SHORT).show();
                            }
                        })
                .show();
        property = null;
    }

    @Override
    protected void onResume() {
        super.onResume();
        locationManager.requestLocationUpdates(provider, 400, 1, this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        locationManager.removeUpdates(this);
    }

    @Override
    public void onLocationChanged(Location location) {
        this.location = location;
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onProviderDisabled(String provider) {
    }
}
