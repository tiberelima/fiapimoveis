package com.tlima.fiapimoveis.listener;

import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.Toast;

import com.tlima.fiapimoveis.R;
import com.tlima.fiapimoveis.activity.FindActivity;
import com.tlima.fiapimoveis.activity.LoginActivity;
import com.tlima.fiapimoveis.activity.MapsActivity_02;
import com.tlima.fiapimoveis.activity.PropertyRegisterActivity;
import com.tlima.fiapimoveis.app.Application;

public class MyOnNavigationItemSelectedListener implements NavigationView.OnNavigationItemSelectedListener {

    private final AppCompatActivity appCompatActivity;
    private final DrawerLayout drawerLayout;

    public MyOnNavigationItemSelectedListener(AppCompatActivity activity, DrawerLayout drawerLayout) {
        this.appCompatActivity = activity;
        this.drawerLayout = drawerLayout;
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem menuItem) {
        menuItem.setChecked(!menuItem.isChecked());
        drawerLayout.closeDrawers();

        switch (menuItem.getItemId()) {
            case R.id.action_property_register:
                appCompatActivity.startActivity(new Intent(appCompatActivity.getApplicationContext(), PropertyRegisterActivity.class));
                return true;
            case R.id.action_map:
                appCompatActivity.startActivity(new Intent(appCompatActivity.getApplicationContext(), MapsActivity_02.class));
                return true;
            case R.id.action_find:
                appCompatActivity.startActivity(new Intent(appCompatActivity.getApplicationContext(), FindActivity.class));
                return true;
            case R.id.action_logoff:
                Intent intent = new Intent(appCompatActivity.getApplicationContext(), LoginActivity.class);
                intent.putExtra(Application.LOGOFF, true);
                appCompatActivity.startActivity(intent);
                appCompatActivity.finish();
                return true;
            default:
                Toast.makeText(appCompatActivity.getApplicationContext(), R.string.somethings_wrong, Toast.LENGTH_SHORT).show();
                return false;
        }
    }
}
